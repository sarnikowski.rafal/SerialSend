﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.IO;
using System.Threading;

namespace SerialSend
{
    public partial class SerialSend : Form
    {
        private Dictionary<string, bool> gpsValuesToShow;

        delegate void SetTextCallback(string text);
        string InputData = String.Empty;

        delegate void WriteFromFile(string text);


        Thread ReadFileThread;
        string FileName;
        int SleepingTime;

        public SerialSend()
        {
            InitializeComponent();
            radioButton1.Select();
            serialPort1.DataReceived +=
              new System.IO.Ports.SerialDataReceivedEventHandler(port_DataReceived_1);
            gpsValuesToShow = new Dictionary<string, bool>();
            gpsValuesToShow.Add("$GPGGA", true);
            gpsValuesToShow.Add("$GPGSA", true);
            gpsValuesToShow.Add("$GPGSV", true);
            gpsValuesToShow.Add("$GPRMC", true);
            gpsValuesToShow.Add("$GPVTG", true);
            gpsValuesToShow.Add("Else", true);
        }

        private void port_DataReceived_1(object sender, SerialDataReceivedEventArgs e)
        {
            InputData = serialPort1.ReadExisting();
            
            if (InputData != String.Empty && InputData[0] == '$')
            {
                List<string> NmeaVals = InputData.Split(',').Select(s => s).ToList();
                if (NmeaVals[0] == "$GPGGA" && !radioButton3.Checked)
                {
                    if(textBox6.InvokeRequired)
                    {
                        textBox6.Invoke((MethodInvoker)delegate { textBox6.Text = NmeaVals[2]; });
                    }
                    if (textBox7.InvokeRequired)
                    {
                        textBox7.Invoke((MethodInvoker)delegate { textBox7.Text = NmeaVals[4]; });
                    }
                    if (textBox8.InvokeRequired)
                    {
                        textBox8.Invoke((MethodInvoker)delegate { textBox8.Text = NmeaVals[9]; });
                    }
                }

                if (NmeaVals[0] == "$RS" && NmeaVals[1] == "2")
                {
                    if (textBox3.InvokeRequired)
                    {
                        textBox3.Invoke((MethodInvoker)delegate { textBox3.Text = NmeaVals[2]; });
                    }
                    if (textBox4.InvokeRequired)
                    {
                        textBox4.Invoke((MethodInvoker)delegate { textBox4.Text = NmeaVals[3]; });
                    }
                    if (textBox5.InvokeRequired)
                    {
                        textBox5.Invoke((MethodInvoker)delegate { textBox5.Text = NmeaVals[4]; });
                    }
                }
                else if (radioButton2.Checked && NmeaVals[1] == "2")
                {
                    if (float.Parse(textBox3.Text) != 0 && float.Parse(NmeaVals[2]) != 0)
                        textBox3.Text = (float.Parse(textBox3.Text) / 2 + float.Parse(NmeaVals[2]) / 2).ToString();
                    else
                        textBox3.Text = (float.Parse(textBox3.Text) + float.Parse(NmeaVals[2])).ToString();

                    if (float.Parse(textBox4.Text) != 0 && float.Parse(NmeaVals[4]) != 0)
                        textBox4.Text = (float.Parse(textBox4.Text) / 2 + float.Parse(NmeaVals[4]) / 2).ToString();
                    else
                        textBox4.Text = (float.Parse(textBox4.Text) + float.Parse(NmeaVals[4])).ToString();

                    if (float.Parse(textBox5.Text) != 0 && float.Parse(NmeaVals[8]) != 0)
                        textBox5.Text = (float.Parse(textBox5.Text) / 2 + float.Parse(NmeaVals[8]) / 2).ToString();
                    else
                        textBox5.Text = (float.Parse(textBox5.Text) + float.Parse(NmeaVals[8])).ToString();
                }
                
            }
            this.BeginInvoke(new SetTextCallback(SetText), new object[] { InputData });
        }

        private void SendText(string text)
        {
            try
            {
                serialPort1.WriteLine(text);
            }
            catch(IOException IoEx)
            {
                MessageBox.Show(IoEx.Message);
            }
        }

        private void SetText(string text)
        {
            try
            {
                if (gpsValuesToShow[text.Substring(0, 6)])
                {
                    richTextBox1.Text += "Recieved: " + text + "\n";
                    richTextBox1.SelectionStart = richTextBox1.Text.Length; //Set the current caret position at the end
                    richTextBox1.ScrollToCaret(); //Now scroll it automatically
                }
            }
            catch(System.Collections.Generic.KeyNotFoundException)
            {
                if(gpsValuesToShow["Else"])
                {
                    richTextBox1.Text += "Recieved: " + text + "\n";
                    richTextBox1.SelectionStart = richTextBox1.Text.Length; //Set the current caret position at the end
                    richTextBox1.ScrollToCaret(); //Now scroll it automatically
                }
            }
        }

        private void SerialSend_Load(object sender, EventArgs e)
        {

        }

        public string ParseGPGGA(string inputData)
        {
            List<string> NmeaVals = inputData.Split(',').Select(s => s).ToList();
            if(NmeaVals[0] == "$GPWPL")
            {
                //$GPGGA,123519,4807.038,N,01131.000,E,1,08,0.9,545.4,M,46.9,M,,*47
                string newData = "$GPGGA,," + NmeaVals[1] + "," + NmeaVals[2] 
                    + "," + NmeaVals[3] + "," + NmeaVals[4] + ",,,,";
                if(checkBox1.Checked)
                {
                    newData += textBox9.Text + ",M,";
                }
                return newData;
            }
            return inputData;
        }

        public void SendDataFromFile(object data)
        {
            using(StreamReader fileStream = new StreamReader(FileName))
            {
                while(!fileStream.EndOfStream)
                {
                    string InputData = ParseGPGGA(fileStream.ReadLine());
                    if (InputData.Length > 63)
                    {
                        //this.BeginInvoke(new SetTextCallback(SendText), new object[] { lel });
                        //kek = kek.Substring(59);
                        string lel = InputData.Substring(0, 63);
                        this.BeginInvoke(new SetTextCallback(SendText), new object[] { lel });
                        InputData = InputData.Substring(63);
                        Thread.Sleep(10);
                    }
                    InputData += "\r";
                    this.BeginInvoke(new SetTextCallback(SendText), new object[] {InputData});
                    Thread.Sleep(SleepingTime);
                }
            }
        }

        public void SendDataCommand(string data)
        {
            if (data.Length > 63)
            {
                string lel;
                //this.BeginInvoke(new SetTextCallback(SendText), new object[] { lel });
                //kek = kek.Substring(59);
                lel = data.Substring(0, 63);
                this.BeginInvoke(new SetTextCallback(SendText), new object[] { lel });
                data = data.Substring(63);
                Thread.Sleep(10);
            }
            data += "\r";
            this.BeginInvoke(new SetTextCallback(SendText), new object[] { data });
            Thread.Sleep(10);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(serialPort1.IsOpen)
            {
                ReadFileThread = new Thread(SendDataFromFile);
                this.FileName = textBox1.Text;
                SleepingTime = int.Parse(textBox2.Text);
                ReadFileThread.Start();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem == null)
            {
                richTextBox1.Text += "No port selected";
                return;
            }  
            richTextBox1.Text += "Starting VCOM on " + listBox1.SelectedItem + "\n";
            serialPort1.PortName = listBox1.SelectedItem.ToString();
            serialPort1.BaudRate = 115200;
            serialPort1.DataBits = 8;
            serialPort1.StopBits = StopBits.One;
            serialPort1.Handshake = Handshake.None;
            serialPort1.Parity = Parity.None;
            try
            {
                serialPort1.Open();
            }
            catch(IOException IoEx) 
            {
                MessageBox.Show(IoEx.Message);
            }
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string[] ArrayComPortsNames = null;

            ArrayComPortsNames = SerialPort.GetPortNames();
            listBox1.DataSource = ArrayComPortsNames.ToList();
            if(ArrayComPortsNames.Length == 0)
            {
                richTextBox1.Text += "No COM ports detected\n";
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                if (serialPort1.IsOpen)
                {
                    serialPort1.Close();
                    listBox1.DataSource = null;
                }
            }
            catch(System.IO.IOException IoEx)
            {
                MessageBox.Show("Problem with Serial Connection\nError: " + IoEx.Message);
            }
            
        }

        private void button5_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.Filter = "Text Files (.txt)|*.txt|All Files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.Multiselect = false;

            var userClickedOK = openFileDialog1.ShowDialog();
            if(userClickedOK == DialogResult.OK)
            {
                textBox1.Text +=
                openFileDialog1.FileName;
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
        }

        private void checkSumToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Checksum wind = new Checksum();
            wind.Show();
        }

        private void gPSOutputToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GpsOptions gpsOptions = new GpsOptions(gpsValuesToShow);
            gpsOptions.ShowDialog(this);
            gpsValuesToShow = gpsOptions.GpsOptionsValues;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                textBox3.Clear();
                textBox4.Clear();
                textBox5.Clear();

                if(serialPort1.IsOpen)
                {
                    SendDataCommand("$RS,1,");
                }
            }
            catch(IOException IoEx)
            {
                MessageBox.Show(IoEx.Message);
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (serialPort1.IsOpen)
                {
                    SendDataCommand("$RS,2,");
                }
            }
            catch (IOException IoEx)
            {
                MessageBox.Show(IoEx.Message);
            }
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                textBox3.Clear();
                textBox4.Clear();
                textBox5.Clear();
                if (serialPort1.IsOpen)
                {
                    string cmd = "$RS,3,";
                    cmd += textBox6.Text + ",N,";
                    cmd += textBox7.Text + ",E,";
                    cmd += textBox8.Text;
                    SendDataCommand(cmd);

                }
            }
            catch (IOException IoEx)
            {
                MessageBox.Show(IoEx.Message);
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

    }
}
