﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SerialSend
{
    public partial class GpsOptions : Form
    {

        public Dictionary<string, bool> GpsOptionsValues{get; set;}
        public GpsOptions()
        {
            InitializeComponent();
        }

        public GpsOptions(Dictionary<string, bool> values)
        {
            InitializeComponent();
            for(int i = 0; i < checkedListBox1.Items.Count; ++i)
            {
                 checkedListBox1.SetItemChecked(i, values[checkedListBox1.Items[i].ToString()]);
            }
            GpsOptionsValues = values;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            for(int i = 0; i < checkedListBox1.Items.Count; ++i)
            {
                GpsOptionsValues[checkedListBox1.Items[i].ToString()] = checkedListBox1.GetItemChecked(i);
            }
            this.Close();
        }
    }
}
